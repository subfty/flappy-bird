﻿using UnityEngine;
using System.Collections;

public class SceneRoot : Singleton<SceneRoot> {

    #region String constants
    public const string GAME_LEVEL = "game_level";
    public const string DIALOG_ZOOM_IN = "ui/DialogZoomIn";
    public const string UI = "ui";
    public const string DIALOG_PATH = "Dialogs/";

    public const string TILED_LEVELS_PATH = "Levels/Tiled2Unity/Prefabs/";
    public const string DATA_LEVELS_PATH = "Levels/";
    #endregion

    #region Numeric constants
    public const float MAX_LEVEL_HEIGHT = 10000.0f;    
    public const float OBSTRUCTIVE_ZONE = 100.0f;
    #endregion

    public RectTransform UIRoot;

    void Start () {}		
	void Update () {}
}
