﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class EMonoBehaviour : MonoBehaviour {

    [Header("Data")]
    [SerializeField]
    protected string _Name = "";

    private EntityContainer _EContainer;
    protected bool _DestroyCalled;
    private Vector3 _PositionOnTile;

    #region Properties
    public string CharacterName {
        get { return _Name; }
    }

    public Vector3 PositionOnTile {
        get { return _PositionOnTile; }

        set { _PositionOnTile = value; }
    }

    public EntityContainer EContainer {
        get {
            if (_DestroyCalled) return null;

            if (_EContainer == null)
                _EContainer = GetComponent<EntityContainer>();
            return _EContainer;
        }
    }   
    #endregion

    protected virtual void Awake() {        
        TinyTokenManager
            .Instance
            .Register<Msg.NotifyEntity>(
                this, 
                (m) => {            
                    if (EContainer == null) return;
                    if (!EContainer.Entity.hasTag) return;
                    if (!EContainer.Entity.tag.Tag.Equals(m.EntityTag)) return;

                    OnNotify(m.Parameters);
                });
    }

    protected virtual void OnDestroy() {
        _DestroyCalled = true;
        TinyTokenManager
            .Instance
            .UnregisterAll(this);
    }

    protected virtual void DestroySelf() {
        if (!_DestroyCalled) {
            EContainer.Entity.IsDestroy(true);            
            _DestroyCalled = true;
        }
    }

    protected void SetTag(string tag) {
        if (!_DestroyCalled) {
            EContainer.Entity.ReplaceTag(tag);
        }
    }

    protected virtual void OnNotify(string[] parameters) { }
}
