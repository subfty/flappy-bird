﻿using Entitas;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class InputReceiverComponent : IComponent {    
    public float HoldFillSpeed;    
    public float ReleaseFillSpeed;
    
    public float TouchHold;
    public bool JustPressed;
    public bool JustReleased;
}

namespace Entitas {
    public partial class Entity {
        public Entity AddInputReceiver() {
            return AddInputReceiver(0, 0, 0, false, false);            
        }
        public Entity ReplaceInputReceiver() {
            return ReplaceInputReceiver(0, 0, 0, false, false);
        }        
    }
}