﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class FlappyBird : FightPlaceholder {
    #region Game Object bindings
    [Header("Resources")]
    [SerializeField]
    private GameObject _Resources;
    [SerializeField]
    private GameObject _ObstaclePrefab;
    #endregion

    #region Config
    [Header("Config")]
    [SerializeField]    
    private DiffRangeProperty _ObstacleSpeed = new DiffRangeProperty(250.0f, 150.0f);
    [SerializeField]    
    private DiffRangeProperty _BaseSpawnDelay = new DiffRangeProperty(2.0f, -1.0f);
    [SerializeField]
    [Range(0, 1)]
    private float _MaxSpawnDelayVariance = 0.2f;
    [SerializeField]    
    DiffRangeProperty _JumpAcc = new DiffRangeProperty(0.1f, 0.7f); //0.8
    [SerializeField]    
    DiffRangeProperty _Gravity = new DiffRangeProperty(0.8f, 0.7f);

    [SerializeField]
    [Range(0, FightPlaceholder.PLAY_AREA_WIDTH)]
    float _StartDotPosX = 100;
    [SerializeField]
    float _StartDotPosY = 300;
    [SerializeField]    
    DiffRangeProperty _MinHoleSize = new DiffRangeProperty(144, 0); // 144
    [SerializeField]
    DiffRangeProperty _MaxHoleSize = new DiffRangeProperty(207, 0); // 207
    [SerializeField]
    DiffRangeProperty _MinHoleYPos = new DiffRangeProperty(140, 0); // 140
    [SerializeField]
    DiffRangeProperty _MaxHoleYPos = new DiffRangeProperty(350, 0); // 350    
    #endregion   

    #region Properties
    float ObstacleSpeed {
        get { return _ObstacleSpeed.GetValue(GameDifficulty); }
    }

    float SpawnTime {
        get { return _SpawnDelay; }
    }    

    float DotRadius {
        get { return 20.0f; }
    }

    float Gravity {
        get { return -_Gravity.GetValue(GameDifficulty) * 1000.0f; }
    }

    float JumpAcc {
        get {
            return _JumpAcc.GetValue(GameDifficulty) * 1000.0f;
        }
    }
    #endregion

    #region Pools
    List<RectTransform> _ObstaclesPool;
    #endregion
    
    FightDot _Dot;
    RectTransform _TDot;
    float _DotYVeloc;
    float _LastSpawned;
    float _SpawnDelay;

    void RandNewSpawnDelay() {
        _SpawnDelay = 
            _BaseSpawnDelay.GetValue(GameDifficulty) + 
            _MaxSpawnDelayVariance * UnityEngine.Random.Range(0.0f, 1.0f);
    }

    protected override void Start() {
        base.Start();

        // initialisation
        if (_Dot == null) {
            _Resources.gameObject.SetActive(false);

            _Dot = SpawnDot();
            _Dot.transform.SetParent(this.transform, false);
            _TDot = _Dot.GetComponent<RectTransform>();

            _ObstaclesPool = new List<RectTransform>();
        }

        if (_GemTypes != null)
            _Dot.SetGemTypes(_GemTypes);

        _TDot.anchoredPosition = new Vector2(
            _StartDotPosX, 
            _StartDotPosY);
        _DotYVeloc = 0.0f;
    }

    protected override void OnDestroy() {
        base.OnDestroy();

        RecyclePool<RectTransform>(_ObstaclesPool);
    }

    protected override void GameUpdate() {
        base.GameUpdate();

        float delta = Time.deltaTime;

        // moving obstacles
        foreach (RectTransform o in _ObstaclesPool) {
            if (!o.gameObject.activeSelf) continue;

            RectTransform t = o.GetComponent<RectTransform>();

            t.anchoredPosition =
                new Vector2(
                t.anchoredPosition.x - delta * ObstacleSpeed,
                t.anchoredPosition.y);

            if (t.anchoredPosition.x + t.sizeDelta.x < 0)
                Recycle<RectTransform>(o);
        }

        // TODO: spawning obstacles    
        // spawning obstacles
        _LastSpawned += delta;
        while (_LastSpawned > SpawnTime) {
            _LastSpawned -= SpawnTime;

            float holeY = UnityEngine.Random.Range(
                _MinHoleYPos.GetValue(GameDifficulty), 
                _MaxHoleYPos.GetValue(GameDifficulty));
            float holeWidth = UnityEngine.Random.Range(
                _MinHoleSize.GetValue(GameDifficulty),
                _MaxHoleSize.GetValue(GameDifficulty));

            RectTransform obstacle = GetRecycled<RectTransform>(_ObstaclesPool, ObstacleFactory).GetComponent<RectTransform>();
            ObstacleInit(obstacle, FightPlaceholder.PLAY_AREA_WIDTH, holeY, false);

            obstacle = GetRecycled<RectTransform>(_ObstaclesPool, ObstacleFactory).GetComponent<RectTransform>();
            ObstacleInit(obstacle, FightPlaceholder.PLAY_AREA_WIDTH, holeY + holeWidth, true);                            

            RandNewSpawnDelay();
        }


        if (JustPressed) {
            _DotYVeloc = JumpAcc;
        }

        _DotYVeloc += Gravity * delta;
        float newDotPosY = _TDot.anchoredPosition.y;
        newDotPosY += delta * _DotYVeloc;

        newDotPosY = Mathf.Clamp(newDotPosY, 0.0f, FightPlaceholder.PLAY_AREA_HEIGHT);

        _TDot.anchoredPosition = 
            new Vector2(
                _TDot.anchoredPosition.x,
                newDotPosY);

        // checking for bounds collisions
        if (_TDot.anchoredPosition.y <= 0.0f || _TDot.anchoredPosition.y >= FightPlaceholder.PLAY_AREA_HEIGHT) {
            GameFailed();
            return;
        }

        // checking for collisions
        foreach (RectTransform opponent in _ObstaclesPool) {
            if (!opponent.gameObject.activeSelf) continue;
            RectTransform t = opponent.GetComponent<RectTransform>();

            Rect r = new Rect(
                t.anchoredPosition.x,
                t.anchoredPosition.y,
                t.sizeDelta.x,
                t.sizeDelta.y);
            if (r.Contains(_TDot.anchoredPosition)) {
                GameFailed();
                break;
            }
        }
    }

    RectTransform ObstacleFactory() {
        RectTransform obst = Instantiate(_ObstaclePrefab).GetComponent<RectTransform>();
        obst.transform.SetParent(this.transform, false);
        return obst;
    }

    RectTransform ObstacleInit(RectTransform obstacle, float posX, float edgePosY, bool ceiling) {        
        if (!ceiling)
            edgePosY = edgePosY - obstacle.sizeDelta.y;

        obstacle.anchoredPosition = new Vector2(
            posX,
            edgePosY);

        // TODO: hack to ensure it's visible
        obstacle.gameObject.SetActive(false);
        obstacle.gameObject.SetActive(true);

        return obstacle;
    }
}
