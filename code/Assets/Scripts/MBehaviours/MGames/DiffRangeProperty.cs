﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class DiffRangeProperty {
    [SerializeField]
    protected float _Value;

    [SerializeField]
    protected float _ValueDelta;

    #region Properties
    public float Value {
        private set { }
        get {return _Value;}
    }

    public float ValueDelta {
        private set { }
        get { return _ValueDelta; }
    }
    #endregion

    public DiffRangeProperty() {

    }

    public DiffRangeProperty(float value, float valueDelta) {
        _Value = value;
        _ValueDelta = valueDelta;
    }

    public float GetValue(float diff) {
        return _Value + diff * _ValueDelta;
    }
}
