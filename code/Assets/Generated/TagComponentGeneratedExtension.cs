using System.Collections.Generic;

namespace Entitas {
    public partial class Entity {
        public TagComponent tag { get { return (TagComponent)GetComponent(ComponentIds.Tag); } }

        public bool hasTag { get { return HasComponent(ComponentIds.Tag); } }

        static readonly Stack<TagComponent> _tagComponentPool = new Stack<TagComponent>();

        public static void ClearTagComponentPool() {
            _tagComponentPool.Clear();
        }

        public Entity AddTag(string newTag) {
            var component = _tagComponentPool.Count > 0 ? _tagComponentPool.Pop() : new TagComponent();
            component.Tag = newTag;
            return AddComponent(ComponentIds.Tag, component);
        }

        public Entity ReplaceTag(string newTag) {
            var previousComponent = hasTag ? tag : null;
            var component = _tagComponentPool.Count > 0 ? _tagComponentPool.Pop() : new TagComponent();
            component.Tag = newTag;
            ReplaceComponent(ComponentIds.Tag, component);
            if (previousComponent != null) {
                _tagComponentPool.Push(previousComponent);
            }
            return this;
        }

        public Entity RemoveTag() {
            var component = tag;
            RemoveComponent(ComponentIds.Tag);
            _tagComponentPool.Push(component);
            return this;
        }
    }

    public partial class Matcher {
        static IMatcher _matcherTag;

        public static IMatcher Tag {
            get {
                if (_matcherTag == null) {
                    var matcher = (Matcher)Matcher.AllOf(ComponentIds.Tag);
                    matcher.componentNames = ComponentIds.componentNames;
                    _matcherTag = matcher;
                }

                return _matcherTag;
            }
        }
    }
}
