using System.Collections.Generic;

namespace Entitas {
    public partial class Entity {
        public UIPositionComponent uIPosition { get { return (UIPositionComponent)GetComponent(ComponentIds.UIPosition); } }

        public bool hasUIPosition { get { return HasComponent(ComponentIds.UIPosition); } }

        static readonly Stack<UIPositionComponent> _uIPositionComponentPool = new Stack<UIPositionComponent>();

        public static void ClearUIPositionComponentPool() {
            _uIPositionComponentPool.Clear();
        }

        public Entity AddUIPosition(float newX, float newY, float newZ, bool newWorldPos) {
            var component = _uIPositionComponentPool.Count > 0 ? _uIPositionComponentPool.Pop() : new UIPositionComponent();
            component.X = newX;
            component.Y = newY;
            component.Z = newZ;
            component.WorldPos = newWorldPos;
            return AddComponent(ComponentIds.UIPosition, component);
        }

        public Entity ReplaceUIPosition(float newX, float newY, float newZ, bool newWorldPos) {
            var previousComponent = hasUIPosition ? uIPosition : null;
            var component = _uIPositionComponentPool.Count > 0 ? _uIPositionComponentPool.Pop() : new UIPositionComponent();
            component.X = newX;
            component.Y = newY;
            component.Z = newZ;
            component.WorldPos = newWorldPos;
            ReplaceComponent(ComponentIds.UIPosition, component);
            if (previousComponent != null) {
                _uIPositionComponentPool.Push(previousComponent);
            }
            return this;
        }

        public Entity RemoveUIPosition() {
            var component = uIPosition;
            RemoveComponent(ComponentIds.UIPosition);
            _uIPositionComponentPool.Push(component);
            return this;
        }
    }

    public partial class Matcher {
        static IMatcher _matcherUIPosition;

        public static IMatcher UIPosition {
            get {
                if (_matcherUIPosition == null) {
                    var matcher = (Matcher)Matcher.AllOf(ComponentIds.UIPosition);
                    matcher.componentNames = ComponentIds.componentNames;
                    _matcherUIPosition = matcher;
                }

                return _matcherUIPosition;
            }
        }
    }
}
